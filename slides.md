% [Mastodon](https://joinmastodon.org/) bots mit [GitLab](https://about.gitlab.com/)
% [Winnie](https://winniehell.de)
% 2020-01-12

## Worum geht's?

- Beispielbot
- Motivation
- Pipeline schedules in GitLab
- easy-toot / easy-tweet
- Mastodon-Bot
- Live Demo?
- Alternative

# Beispielbot

---

![Screenshot of toot by bot](img/screenshot-comic-bot.png)

---

- dreimal pro Woche neuer Comic
- auf Twitter: [\@bastianmelnyk](https://twitter.com/bastianmelnyk/)
- auf Mastodon: [\@bastianmelnyk\@mastodon.social](https://mastodon.social/@bastianmelnyk)
- vollständig automatisiert: <https://gitlab.com/fonflatter/bots/comic>
- läuft seit August 2019

# Motivation

## Anforderungen

- feste Verbreitungszeitpunkte brauchen Server
- Monitoring / Logging wäre nett
- Weboberfläche erleichtert Unterwegsität

## gitlab.com

- bietet kostenlose Server
- Pipelines haben Logs
- …und E-Mail-Benachrichtigungen
- Config kann im Browser geändert werden
- Pipelines können gestartet/gestoppt werden

# Pipeline schedules

## [Pipelines](https://docs.gitlab.com/ee/ci/pipelines.html)

- Sammlung von Jobs
- jeder Job
  - läuft in definierter Umgebug
  - auf einem Server von GitLab
  - führt beliebige Programme aus
- analog zu mehrere Tabs in Konsole

## [Pipeline schedules](https://docs.gitlab.com/ee/user/project/pipelines/schedules.html)

- Pipelines die zu festem Zeitpunkt laufen
- Beispiel: <https://gitlab.com/winnie-demos/pipeline-schedules>

# easy-toot / <br> easy-tweet

---

- starke Vereinfachung der Twitter / Mastodon API
- erwartet Token und Eingabedatei
- Mastodon: <https://gitlab.com/winniehell/easy-toot/>
- Twitter: <https://gitlab.com/winniehell/easy-tweet/>

# Mastodon-Bot

# Live Demo? 🙀

# Alternative

## [monit](https://mmonit.com/monit/)

- start/stop von bot
- logs lesen
- E-Mail-Benachrichtigungen
- braucht eigenen Server
- keine Möglichkeit Konfiguration zu bearbeiten

# fertsch.
