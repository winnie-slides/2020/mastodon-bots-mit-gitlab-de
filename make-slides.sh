#!/usr/bin/env sh

set -o errexit
set -o nounset

pandoc \
  --standalone \
  --mathjax \
  --to=revealjs \
  --variable=theme:white \
  --variable=backgroundTransition:none \
  --css=style.css \
  --output=index.html \
  --slide-level=2 \
  -V autoPlayMedia=true \
  slides.md
