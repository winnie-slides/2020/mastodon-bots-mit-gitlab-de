# Mastodon bots mit GitLab

## License

All content under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

The [logo on the title slide](img/gitlab-mastodon-bot.png) is derived work from [@dasMaichen](https://dasmaichen.de), [GitLab](https://about.gitlab.com), and the Mastodon Press kit.
